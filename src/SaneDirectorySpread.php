<?php


namespace QBNK\Doctrine\Common\Cache;


trait SaneDirectorySpread {

	protected $directorySpread = 2;

	public function getDirectorySpread() {
		return $this->directorySpread;
	}

	public function setDirectorySpread($spread) {
		$this->directorySpread = (int)$spread;
	}

}