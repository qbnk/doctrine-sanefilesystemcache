<?php


namespace QBNK\Doctrine\Common\Cache;


use FilesystemIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

trait SaneDirectoryPurge {

	protected function purgeEmptyDirectories($directory) {
		if (rand(0, 99) == 50) {
			flush();
			$iterator = new \RecursiveIteratorIterator(
				new \RecursiveDirectoryIterator(
					$directory,
					FilesystemIterator::KEY_AS_PATHNAME
					| FilesystemIterator::CURRENT_AS_FILEINFO
					| FilesystemIterator::SKIP_DOTS
				),
				RecursiveIteratorIterator::CHILD_FIRST
			);
			/** @var SplFileInfo $entry */
			foreach ($iterator as $entry) {
				if ($entry->isDir()) {
					if (count(glob($entry->getPathname() . '/*', GLOB_NOSORT)) === 0) {
						rmdir($entry->getPathname());
					}
				}
			}
		}
	}
}