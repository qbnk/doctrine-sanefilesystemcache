<?php


namespace QBNK\Doctrine\Common\Cache;


class FilesystemCache extends \Doctrine\Common\Cache\FilesystemCache {

	use GetSaneFilename;
	use SaneDirectorySpread;
	use SaneDirectoryPurge;

	protected $toDelete;

	public function __construct($directory, $extension = self::EXTENSION) {
		parent::__construct($directory, $extension);
		$this->toDelete = [];
	}

	/**
	 * {@inheritdoc}
	 */
	protected function doFetch($id)
	{
		$data     = '';
		$lifetime = -1;
		$filename = $this->getFilename($id);

		if ( ! is_file($filename)) {
			return false;
		}

		$resource = fopen($filename, "r");

		if (false !== ($line = fgets($resource))) {
			$lifetime = (integer) $line;
		}

		if ($lifetime !== 0 && $lifetime < time()) {
			fclose($resource);
			$this->toDelete[] = $id;
			return false;
		}

		while (false !== ($line = fgets($resource))) {
			$data .= $line;
		}

		fclose($resource);

		return unserialize($data);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function doContains($id)
	{
		$lifetime = -1;
		$filename = $this->getFilename($id);

		if ( ! is_file($filename)) {
			return false;
		}

		$resource = fopen($filename, "r");

		if (false !== ($line = fgets($resource))) {
			$lifetime = (integer) $line;
		}

		fclose($resource);

		$contains = $lifetime === 0 || $lifetime > time();
		if (!$contains) {
			$this->toDelete[] = $id;
		}
		return $contains;
	}

	protected function doSave($id, $data, $lifeTime = 0) {
		$return = parent::doSave($id, $data, $lifeTime);
		$key = array_search($id, $this->toDelete);
		if ($key !== false) {
			unset($this->toDelete[$key]);
		}
		return $return;
	}

	public function __destruct() {
		if (!empty($this->toDelete)) {
			foreach ($this->toDelete as $id) {
				$this->doDelete($id);
			}
		}
		$this->purgeEmptyDirectories($this->directory);
	}
}