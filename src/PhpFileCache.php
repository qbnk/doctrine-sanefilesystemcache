<?php


namespace QBNK\Doctrine\Common\Cache;

class PhpFileCache extends \Doctrine\Common\Cache\PhpFileCache {

	use GetSaneFilename;
	use SaneDirectorySpread;
	use SaneDirectoryPurge;

	protected $toDelete;

	public function __construct($directory, $extension = self::EXTENSION) {
		parent::__construct($directory, $extension);
		$this->toDelete = [];
	}

	/**
	 * {@inheritdoc}
	 */
	protected function doFetch($id)
	{
		$filename = $this->getFilename($id);

		if ( ! is_file($filename)) {
			return false;
		}

		$value = include $filename;

		if ($value['lifetime'] !== 0 && $value['lifetime'] < time()) {
			$this->toDelete[] = $id;
			return false;
		}

		return $value['data'];
	}

	/**
	 * {@inheritdoc}
	 */
	protected function doContains($id)
	{
		$filename = $this->getFilename($id);

		if ( ! is_file($filename)) {
			return false;
		}

		if ( ! is_readable($filename)) {
			$this->toDelete[] = $id;
			return false;
		}

		$value = include $filename;

		$contains = $value['lifetime'] === 0 || $value['lifetime'] > time();
		if (!$contains) {
			$this->toDelete[] = $id;
		}
		return $contains;
	}

	protected function doSave($id, $data, $lifeTime = 0) {
		$return = parent::doSave($id, $data, $lifeTime);
		$key = array_search($id, $this->toDelete);
		if ($key !== false) {
			unset($this->toDelete[$key]);
		}
		return $return;
	}

	public function __destruct() {
		if (!empty($this->toDelete)) {
			foreach ($this->toDelete as $id) {
				$this->doDelete($id);
			}
		}
		$this->purgeEmptyDirectories($this->directory);
	}
}