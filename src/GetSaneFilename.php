<?php

namespace QBNK\Doctrine\Common\Cache;

trait GetSaneFilename
{
    protected function getFilename($id)
    {
        return $this->getDirectory()
        . DIRECTORY_SEPARATOR
        . implode(DIRECTORY_SEPARATOR, str_split(substr(sha1($id), 0, $this->getDirectorySpread() * 2), 2))
        . DIRECTORY_SEPARATOR
        . preg_replace(['/\-/', '/[^a-zA-Z0-9\-_\[\]]/'], ['__', '-'], $id)
        . $this->getExtension();
    }
}
