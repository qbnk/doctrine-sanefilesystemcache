# Sane Filesystem Cache for Doctrine

As the default `FilesystemCache` and `PhpFileCache` in Doctrine creates an insane amount of folders when storing your 
data, it eats up a lot of inodes. It also never deletes any file regardless if it is expired. This library provides a 
drop-in replacement which provides a sane default (2 folders deep) and supports setting your depth. It will also delete 
no longer needed files.

    $cache = new \QBNK\Doctrine\Common\Cache\PhpFileCache(DIRECTORY);
    $cache->setDirectorySpread(3);	// If not set, defaults to 2.
    $cache->save(CACHE_KEY, CACHE_DATA);
    
## Installation

Add the custom packages repositories to your `composer.json` if you do not already have it.

    {
        "repositories": [
            {
                "type": "composer",
                "url": "http://packages.kaigan.se"
            }
        ]
    }
    
Then add this library to your `composer.json`.

    {
        "require": {
            "qbnk/doctrine-sanefilesystemcache": "dev-master"
        }
    }
    
Lastly run `composer update qbnk/doctrine-sanefilesystemcache` to install it.